﻿using System;
using System.Collections.Generic;

namespace HelloWorld
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            int age = 9;
            Console.WriteLine("My age is"); Console.WriteLine(age);
            var agetwo = 9.1;
            Console.WriteLine(agetwo);
            var name = "Richard";
            Console.WriteLine(name);

            List<int> list = new List<int>();
            list.Add(2);
            list.Add(3);
            list.Add(4);
            list.Add(5);

            foreach (int i in list)
            {
                Console.WriteLine(i);
            }
            long myNum = 15000000000L;
            Console.WriteLine(myNum);

            float MyNum = 5.75F;
            Console.WriteLine(myNum);

            double lol = 19.99D;
            Console.WriteLine(myNum);

            bool isCSharpFun = true;
            bool isFishTasty = false;
            Console.WriteLine(isCSharpFun);   // Outputs True
            Console.WriteLine(isFishTasty);   // Outputs False

            char myGrade = 'B';
            Console.WriteLine(myGrade);






        }
    }
}
